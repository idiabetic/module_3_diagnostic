require 'rails_helper'

describe 'Search' do
  it 'should complete the user story' do
    visit '/'

    fill_in :q, with: '80203'

    VCR.use_cassette("80203") do
      click_on 'Locate'
    end

    expect(current_path).to eq '/search'

    within('#station-66897') do
      expect(page).to have_content('UDR')
      expect(page).to have_content("800 Acoma St")
      expect(page).to have_content("ELEC")
      expect(page).to have_content(0.31422)
      expect(page).to have_content("24 hours daily")
    end

    expect('UDR').to appear_before('PUBLIC STATIONS')

  end
end
