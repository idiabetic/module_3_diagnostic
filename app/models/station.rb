class Station
  attr_reader :id,
              :name,
              :address,
              :fuel_type,
              :distance,
              :access_time

  def initialize(station_data)
    @id          = station_data['id']
    @name        = station_data['station_name']
    @address     = station_data['street_address']
    @fuel_type   = station_data['fuel_type_code']
    @distance    = station_data['distance'].to_f
    @access_time = station_data['access_days_time']
  end
end
