class StationsContainer
  attr_reader :stations

  def initialize(zip_code)
    @stations = sort_stations(zip_code)[0...10]
  end

  def parse(stations_response)
    JSON.parse(stations_response.body)['fuel_stations']
  end

  def get_stations(zip_code)
    parse(ApiInterface.new(zip_code).get_by_location)
  end

  def build_stations(sorted_stations)
    sorted_stations.map do |station|
      Station.new(station)
    end
  end

  def sort_stations(zip_code)
    raw_stations = get_stations(zip_code)
    raw_stations.sort_by! do |station|
      station['distance']
    end
    build_stations(raw_stations)
  end
end
