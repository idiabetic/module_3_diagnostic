class ApiInterface

  def initialize(zip_code)
    @zip_code = zip_code
  end

  def conn
    Faraday.new(:url => 'https://developer.nrel.gov/api/alt-fuel-stations/v1/nearest.json?') do |f|
     f.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end
  end

  def get_by_location
    conn.get do |req|
      req.headers['X-Api-Key'] = 'tDFDkmxSI3GsW0C9FHDp8v4H6TzLPl4o9riVb2GS'
      req.params['location'] = @zip_code
      req.params['radius'] = 6
      req.params['fuel_type'] = 'ELEC, LPG'
    end
  end

end
