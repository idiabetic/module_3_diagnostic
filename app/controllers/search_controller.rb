class SearchController < ApplicationController
  def index
    @stations = StationsContainer.new(params['q']).stations
  end
end
